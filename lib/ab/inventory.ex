defmodule Ab.Inventory do
  use GenServer

  def start_link(root) do
    GenServer.start_link(__MODULE__, root, name: __MODULE__)
  end

  def list(inventory) do
    GenServer.call(__MODULE__, {:list, inventory})
  end

  # Callbacks

  def init(root) do
    {:ok, %{root: root, cache: %{}}}
  end

  def handle_call({:list, inventory}, _from, %{root: root, cache: cache} = state) do
    fun = fn ->
      {stdout, 0} =
        System.cmd(Path.join(root, "venv/bin/ansible-inventory"), ["-i", inventory, "--list"],
          cd: Path.join(root, "ansible")
        )

      Jason.decode!(stdout)
    end

    cache = Map.put_new_lazy(cache, inventory, fun)

    {:reply, {:ok, Map.get(cache, inventory)}, Map.put(state, :cache, cache)}
  end
end
