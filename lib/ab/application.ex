defmodule Ab.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      AbWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Ab.PubSub},
      # Start the Endpoint (http/https)
      AbWeb.Endpoint,
      # Start a worker by calling: Ab.Worker.start_link(arg)
      # {Ab.Worker, arg}
      {Ab.Inventory, :code.priv_dir(:ab)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ab.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    AbWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
