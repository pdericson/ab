defmodule Ab.InventoryTest do
  use ExUnit.Case

  test "list" do
    {:ok, data} = Ab.Inventory.list("inventory/local/hosts.ini")

    assert Map.get(data, "_meta") |> Map.get("hostvars") |> Map.get("node1")

    {:ok, _} = Ab.Inventory.list("inventory/local/hosts.ini")
    {:ok, _} = Ab.Inventory.list("inventory/local/hosts.ini")
    {:ok, _} = Ab.Inventory.list("inventory/local/hosts.ini")
    {:ok, _} = Ab.Inventory.list("inventory/local/hosts.ini")
    {:ok, _} = Ab.Inventory.list("inventory/local/hosts.ini")
  end
end
